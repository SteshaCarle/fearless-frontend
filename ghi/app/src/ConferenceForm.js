import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            });
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json();

            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                const option = document.createElement('option')

                option.value = location.id
                option.innerHTML = location.name
                selectTag.appendChild(option)
            }
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // const handleNameChange = (event) => {
    //     const value = event.target.value;
    //     setName(value);
    // }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                <textarea onChange={handleFormChange} value={formData.description} className="form-control" required type="text" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Max presentations" required type="number" min="1" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.max_attendees} placeholder="Max attendees" required type="number" min="1" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
